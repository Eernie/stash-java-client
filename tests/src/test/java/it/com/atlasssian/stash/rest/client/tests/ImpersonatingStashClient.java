package it.com.atlasssian.stash.rest.client.tests;

import com.atlassian.bamboo.applinks.ImpersonationService;
import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.RepositorySshKey;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;

public class ImpersonatingStashClient implements StashClient {
    private final StashClient stashService;
    private final ImpersonationService impersonationService;
    private final String username;

    public ImpersonatingStashClient(StashClient stashService, ImpersonationService impersonationService, String username) {
        this.stashService = stashService;
        this.impersonationService = impersonationService;
        this.username = username;
    }

    @Override
    public boolean isUserKey(final @Nonnull String publicKey) {
        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.isUserKey(publicKey);
            }
        });
    }

    @Nonnull
    @Override
    public Page<Repository> getRepositories(
            @Nullable final String projectKey,
            @Nullable final String query, final long start, final long limit) {
        return doImpersonate(new Callable<Page<Repository>>() {
            @Override
            public Page<Repository> call() throws Exception {
                return stashService.getRepositories(projectKey, query, start, limit);
            }
        });
    }

    @Override
    public Repository getRepository(@Nonnull final String projectKey,
                                                                       @Nonnull final String repositorySlug) {
        return doImpersonate(new Callable<Repository>() {
            @Override
            public Repository call() throws Exception {
                return stashService.getRepository(projectKey, repositorySlug);
            }
        });
    }

    @Override
    public boolean isRepositoryKey(final @Nonnull String projectKey,
                                                           final @Nonnull String repositorySlug,
                                                           final @Nonnull String publicKey) {
        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.isRepositoryKey(projectKey, repositorySlug, publicKey);
            }
        });
    }

    @Nonnull
    @Override
    public Map<String, String> getStashApplicationProperties() {
        return doImpersonate(new Callable<Map<String, String>>() {
            @Override
            public Map<String, String> call() throws Exception {
                return stashService.getStashApplicationProperties();
            }
        });
    }

    @Nonnull
    @Override
    public Page<RepositorySshKey> getRepositoryKeys(@Nonnull final String projectKey,
                                                                                                        @Nonnull final String repositorySlug,
                                                                                                        final long start,
                                                                                                        final long limit) {
        return doImpersonate(new Callable<Page<RepositorySshKey>>() {
            @Override
            public Page<RepositorySshKey> call() throws Exception {
                return stashService.getRepositoryKeys(projectKey, repositorySlug, start, limit);
            }
        });
    }

    @Override
    public boolean addUserKey(@Nonnull final String publicKey, @Nullable final String keyLabel) {
        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.addUserKey(publicKey, keyLabel);
            }
        });
    }

    @Override
    public boolean removeUserKey(@Nonnull final String publicKey) {
        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.removeUserKey(publicKey);
            }
        });
    }

    @Override
    public boolean removeUserKey(final long keyId) {

        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.removeUserKey(keyId);
            }
        });
    }

    @Override
    public boolean createProject(final @Nonnull String projectKey, @Nonnull final String name, @Nonnull final String type, @Nonnull final String description) {
        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.createProject(projectKey, name, type, description);
            }
        });
    }

    @Override
    public boolean createRepository(@Nonnull final String projectKey, @Nonnull final String name, @Nonnull final String scmId, final boolean forkable) {
        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.createRepository(projectKey, name, scmId, forkable);
            }
        });
    }

    @Override
    public boolean deleteProject(@Nonnull final String projectKey) {
        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.deleteProject(projectKey);
            }
        });
    }

    @Override
    public boolean deleteRepository(@Nonnull final String projectKey, @Nonnull final String repositorySlug) {
        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.deleteRepository(projectKey, repositorySlug);
            }
        });
    }

    @Nonnull
    @Override
    public Page<Project> getAccessibleProjects(final long start,
                                                                                                   final long limit) {
        return doImpersonate(new Callable<Page<Project>>() {
            @Override
            public Page<Project> call() throws Exception {
                return stashService.getAccessibleProjects(start, limit);
            }
        });
    }

    @Nonnull
    @Override
    public Page<Branch> getRepositoryBranches(@Nonnull final String projectKey,
                                                                                                  @Nonnull final String repositorySlug,
                                                                                                  @Nullable final String query,
                                                                                                  final long start,
                                                                                                  final long limit) {
        return doImpersonate(new Callable<Page<Branch>>() {
            @Override
            public Page<Branch> call() throws Exception {
                return stashService.getRepositoryBranches(projectKey, repositorySlug, query, start, limit);
            }
        });
    }

    @Nullable
    @Override
    public Branch getRepositoryDefaultBranch(@Nonnull final String projectKey, @Nonnull final String repositorySlug)
    {
        return doImpersonate(new Callable<Branch>() {
            @Override
            public Branch call() throws Exception {
                return stashService.getRepositoryDefaultBranch(projectKey, repositorySlug);
            }
        });
    }

    @Override
    public boolean addRepositoryKey(@Nonnull final String projectKey,
                                                            @Nonnull final String repositorySlug,
                                                            @Nonnull final String publicKey,
                                                            @Nullable final String keyLabel,
                                                            @Nonnull final Permission keyPermission) {
        return doImpersonate(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return stashService.addRepositoryKey(projectKey, repositorySlug, publicKey, keyLabel, keyPermission);
            }
        });
    }

    @Nonnull
    @Override
    public Page<UserSshKey> getCurrentUserKeys(final long start, final long limit) {
        return doImpersonate(new Callable<Page<UserSshKey>>() {
            @Override
            public Page<UserSshKey> call() throws Exception {
                return stashService.getCurrentUserKeys(start, limit);
            }
        });
    }

    private <T> T doImpersonate(Callable<T> callable) {
        try {
            return impersonationService.runAsUser(username, callable).call();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Permission> getCurrentUserRepositoryPermission(@Nonnull final String projectKey, @Nonnull final String repositorySlug) {
        return doImpersonate(new Callable<Optional<Permission>>() {
            @Override
            public Optional<Permission> call() throws Exception {
                return stashService.getCurrentUserRepositoryPermission(projectKey, repositorySlug);
            }
        });
    }
}
