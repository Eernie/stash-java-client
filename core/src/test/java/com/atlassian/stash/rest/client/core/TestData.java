package com.atlassian.stash.rest.client.core;

import com.google.common.io.CharStreams;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TestData {
    public static final String REPOS = loadString("repos.json");
    public static final String PROJECTS = loadString("projects.json");
    public static final String REPO_MY_REPO = loadString("repo-my-repo.json");
    public static final String REPO_PERSONAL_FORKED = loadString("repo-personal-forked.json");
    public static final String REPO_CREATE_409 = loadString("repo-create-409.json");
    public static final String REPO_CREATE_401 = loadString("repo-create-401.json");
    public static final String BRANCH = loadString("branch.json");
    public static final String BRANCHES = loadString("branches.json");
    public static final String USER_KEYS = loadString("user-keys.json");
    public static final String USER_KEYS_2 = loadString("user-keys-2.json");

    private static String loadString(String name) {
        try {
            InputStream inputStream = TestData.class.getResourceAsStream(name);
            if (inputStream == null) {
                throw new FileNotFoundException(name);
            }
            return CharStreams.toString(new InputStreamReader(inputStream, "UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
