package com.atlassian.stash.rest.client.core;

import com.atlassian.stash.rest.client.api.StashRestException;
import com.atlassian.stash.rest.client.api.StashUnauthorizedRestException;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.atlassian.stash.rest.client.core.http.HttpExecutor;
import com.atlassian.stash.rest.client.core.http.HttpMethod;
import com.atlassian.stash.rest.client.core.http.HttpRequest;
import com.atlassian.stash.rest.client.core.http.HttpResponseProcessor;
import com.jayway.jsonpath.JsonPath;
import junit.framework.AssertionFailedError;
import org.hamcrest.CoreMatchers;
import org.hamcrest.core.StringEndsWith;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static com.atlassian.stash.rest.client.api.EntityMatchers.branch;
import static com.atlassian.stash.rest.client.api.EntityMatchers.page;
import static com.atlassian.stash.rest.client.api.EntityMatchers.project;
import static com.atlassian.stash.rest.client.api.EntityMatchers.repository;
import static com.atlassian.stash.rest.client.api.EntityMatchers.userSshKey;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StashClientImplTest {
    @Mock
    private HttpExecutor httpExecutor;
    private StashClientImpl stashClient;

    @Before
    public void setUp() throws Exception {
        stashClient = new StashClientImpl(httpExecutor);
    }

    @Test
    public void testGetAccessibleProjects() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(200, TestData.PROJECTS);

        // when
        Page<Project> projectsPage = stashClient.getAccessibleProjects(0, 10);

        // then
        assertThat(projectsPage, page(Project.class)
                        .size(is(1))
                        .limit(is(25))
                        .values(CoreMatchers.<List<Project>>notNullValue())
                        .build()
        );
        assertThat(projectsPage.getValues().get(0), project()
                        .name(is("My Cool Project"))
                        .key(is("PRJ"))
                        .selfUrl(is("http://link/to/project"))
                        .build()
        );
    }

    @Test
    public void testRemoveUserKeyById() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.removeUserKey(123);

        // then

        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        HttpRequest request = captor.getValue();
        assertThat(request.getMethod(), is(HttpMethod.DELETE));
        assertThat(request.getUrl(), StringEndsWith.endsWith("/keys/123"));
        assertThat(result, is(true));
    }

    @Test
    public void testRemoveUserKeyByPublicKeyAndLabel() {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(200, TestData.USER_KEYS)
                .nextResponse(200, TestData.USER_KEYS_2)
                .nextResponse(204);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.removeUserKey("ssh-rsa EEEEEE... he@127.0.0.1");

        // then
        verify(httpExecutor, times(3)).execute(captor.capture(), any(HttpResponseProcessor.class));
        HttpRequest deletingRequest = captor.getAllValues().get(2);
        assertThat(deletingRequest.getUrl(), StringEndsWith.endsWith("/keys/3"));
        assertThat(deletingRequest.getMethod(), equalTo(HttpMethod.DELETE));
        assertThat(result, equalTo(true));
    }

    @Test
    public void testRemoveUserKeyByPublicKeyAndLabel_shouldReturnFalseForNonExistingKeys() {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(200, TestData.USER_KEYS)
                .nextResponse(200, TestData.USER_KEYS_2);
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.removeUserKey("ssh-rsa XXXXXX... he@127.0.0.1");

        // then
        verify(httpExecutor, times(2)).execute(captor.capture(), any(HttpResponseProcessor.class)); // in this situation implementation asked for two pages but there was no such key
        assertThat(result, equalTo(false));
    }

    @Test
    public void testCreateProject() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.createProject("KEY", "NAME", "NORMAL", "DESC");

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        String payload = captor.getValue().getPayload();
        assertThat(JsonPath.<String>read(payload, "$.key"), is("KEY"));
        assertThat(JsonPath.<String>read(payload, "$.name"), is("NAME"));
        assertThat(JsonPath.<String>read(payload, "$.type"), is("NORMAL"));
        assertThat(JsonPath.<String>read(payload, "$.description"), is("DESC"));
        assertThat(result, is(true));
    }

    @Test
    public void testCreateProject_should409OnConflict() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(409, TestData.REPO_CREATE_409);

        // when
        try {
            stashClient.createProject("KEY", "NAME", "NORMAL", "DESCRIPTION");
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(409));
        }
    }

    @Test
    public void testCreateProject_should401WhenUnauthorized() {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(401, TestData.REPO_CREATE_401);

        // when
        try {
            stashClient.createProject("KEY", "NAME", "NORMAL", "DESCRIPTION");
            // then
            throw new AssertionFailedError("Expected exception " + StashRestException.class.getName());
        } catch (StashRestException e) {
            assertThat(e.getStatusCode(), is(401));
        }
    }

    @Test
    public void testDeleteProject() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.deleteProject("PRJ");

        // then

        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        HttpRequest request = captor.getValue();
        assertThat(request.getMethod(), is(HttpMethod.DELETE));
        assertThat(request.getUrl(), StringEndsWith.endsWith("/projects/PRJ"));
        assertThat(result, is(true));
    }


    @Test
    public void testCreateRepository() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.createRepository("KEY", "SLUG", "git", false);

        // then
        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        HttpRequest request = captor.getValue();
        String payload = request.getPayload();
        String url = request.getUrl();

        assertThat(url, StringEndsWith.endsWith("/projects/KEY/repos"));
        assertThat(JsonPath.<String>read(payload, "$.name"), is("SLUG"));
        assertThat(JsonPath.<String>read(payload, "$.scmId"), is("git"));
        assertThat(JsonPath.<Boolean>read(payload, "$.forkable"), is(false));
        assertThat(result, is(true));
    }

    @Test
    public void testDeleteRepository() {
        // given
        ArgumentCaptor<HttpRequest> captor = ArgumentCaptor.forClass(HttpRequest.class);

        // when
        boolean result = stashClient.deleteRepository("PRJ", "SLUG");

        // then

        verify(httpExecutor).execute(captor.capture(), any(HttpResponseProcessor.class));
        HttpRequest request = captor.getValue();
        assertThat(request.getMethod(), is(HttpMethod.DELETE));
        assertThat(request.getUrl(), StringEndsWith.endsWith("/projects/PRJ/repos/SLUG"));
        assertThat(result, is(true));
    }

    @Test
    public void testGetRepositories() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(200, TestData.REPOS);

        // when
        Page<Repository> repositoriesPage = stashClient.getRepositories(null, null, 0, 10);

        // then
        assertThat(repositoriesPage, page(Repository.class)
                        .size(is(1))
                        .limit(is(25))
                        .values(CoreMatchers.<List<Repository>>notNullValue())
                        .build()
        );
        assertThat(repositoriesPage.getValues().get(0), repository()
                        .slug(is("my-repo"))
                        .name(is("My repo"))
                        .sshCloneUrl(is("ssh://git@<baseURL>/PRJ/my-repo.git"))
                        .httpCloneUrl(is("https://<baseURL>/scm/PRJ/my-repo.git"))
                        .selfUrl(is("http://link/to/repository"))
                        .project(project()
                                        .name(is("My Cool Project"))
                                        .key(is("PRJ"))
                                        .selfUrl(is("http://link/to/project"))
                                        .build()
                        )
                        .build()
        );
    }

    @Test
    public void testGetRepository() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(200, TestData.REPO_MY_REPO);

        // when
        Repository repository = stashClient.getRepository("PRJ", "my-repo");

        // then
        assertThat(repository, repository()
                        .slug(is("my-repo"))
                        .name(is("My repo"))
                        .sshCloneUrl(is("ssh://git@<baseURL>/PRJ/my-repo.git"))
                        .httpCloneUrl(is("https://<baseURL>/scm/PRJ/my-repo.git"))
                        .selfUrl(is("http://link/to/repository"))
                        .project(project()
                                        .name(is("My Cool Project"))
                                        .key(is("PRJ"))
                                        .selfUrl(is("http://link/to/project"))
                                        .isPublic(is(true))
                                        .isPersonal(is(false))
                                        .build()
                        )
                        .origin(CoreMatchers.<Repository>nullValue())
                        .build()
        );
    }

    @Test
    public void testGetRepositoryPersonalForked() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(200, TestData.REPO_PERSONAL_FORKED);

        // when
        Repository repository = stashClient.getRepository("PRJ", "my-repo");

        // then
        assertThat(repository, repository()
                        .origin(repository()
                                        .name(is("confluence"))
                                        .project(project()
                                                        .name(is("Confluence"))
                                                        .build()
                                        )
                                        .build()
                        )
                        .project(project()
                                        .isPersonal(is(true))
                                        .build()
                        )
                        .build()
        );
    }
    @Test
    public void testGetRepositoryIfNotExists() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(404, "");

        // when
        Repository repository = stashClient.getRepository("PRJ", "NON_EXISTING_REPO");

        // then
        assertThat(repository, nullValue());
    }

    @Test
    public void testGetRepositoryBranches() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(200, TestData.BRANCHES);

        // when
        Page<Branch> branchesPage = stashClient.getRepositoryBranches("PRJ", "my-repo", null, 0, 10);

        // then
        assertThat(branchesPage, page(Branch.class)
                        .size(is(1))
                        .limit(is(25))
                        .values(CoreMatchers.<List<Branch>>notNullValue())
                        .build()
        );
        assertThat(branchesPage.getValues().get(0), branch()
                        .id(is("refs/heads/master"))
                        .displayId(is("master"))
                        .latestChangeset(is("8d51122def5632836d1cb1026e879069e10a1e13"))
                        .isDefault(is(true))
                        .build()
        );
    }

    @Test
    public void testGetRepositoryDefaultBranch() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(200, TestData.BRANCH);

        // when
        Branch branch = stashClient.getRepositoryDefaultBranch("PRJ", "my-repo");

        // then
        assertThat(branch, branch()
                .id(is("refs/heads/master"))
                .displayId(is("master"))
                .latestChangeset(is("8d51122def5632836d1cb1026e879069e10a1e13"))
                .isDefault(is(true))
                .build()
        );
    }

    @Test
    public void testGetUserKeys() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(200, TestData.USER_KEYS);

        // when
        Page<UserSshKey> keysPage = stashClient.getCurrentUserKeys(0, 10);

        // then
        assertThat(keysPage, page(UserSshKey.class)
                        .size(is(2))
                        .limit(is(25))
                        .values(CoreMatchers.<List<UserSshKey>>notNullValue())
                        .build()
        );
        assertThat(keysPage.getValues().get(0), userSshKey()
                        .id(is(1L))
                        .text(is("ssh-rsa AAAAB3... me@127.0.0.1"))
                        .label(is("me@127.0.0.1"))
                        .build()
        );
        assertThat(keysPage.getValues().get(1), userSshKey()
                        .id(is(2L))
                        .text(is("AAAAB3...bo2c="))
                        .label(nullValue(String.class))
                        .build()
        );
    }

    @Test
    public void testIsUserKey() throws Exception {
        // given
        stashClient = new StashClientImpl(httpExecutor, 2); // set page limit to 2 to limit number keys to iterate thru
        // first response
        HttpExecutorMock.from(httpExecutor).nextResponse(200, TestData.USER_KEYS_2);
        // second response
        HttpExecutorMock.from(httpExecutor).nextResponse(200, TestData.USER_KEYS);

        // when
        // check key from seconds response
        boolean isKey = stashClient.isUserKey("ssh-rsa AAAAB3... me@127.0.0.1");

        // then
        assertThat(isKey, is(true));
    }

    @Test(expected = StashRestException.class)
    public void testHttpErrorCausesStashRestException() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(400, "");

        // when
        stashClient.getStashApplicationProperties();
    }

    @Test(expected = StashUnauthorizedRestException.class)
    public void testHttpError401CausesStashUnauthorizedRestException() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(401, "");

        // when
        stashClient.getStashApplicationProperties();
    }

    @Test(expected = StashRestException.class)
    public void testGetCurrentUserRepositoryPermissionRepositoryNotExists() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(404, "");

        // when
        stashClient.getCurrentUserRepositoryPermission("FOO", "bar");
    }

    @Test
    public void testGetCurrentUserRepositoryPermissionHasRepoAdminPermission() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor).nextResponse(200, "{message: 'Has REPO_ADMIN permission'}");

        // when
        final Optional<Permission> permission = stashClient.getCurrentUserRepositoryPermission("FOO", "bar");

        // then
        assertThat(permission, equalTo(Optional.of(Permission.REPO_ADMIN)));
    }

    @Test
    public void testGetCurrentUserRepositoryPermissionHasRepoReadPermission() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(401, "{error : [{message:'No REPO_ADMIN permission'}]}")
                .nextResponse(200, "{message: 'Has REPO_READ permission'}");

        // when
        final Optional<Permission> permission = stashClient.getCurrentUserRepositoryPermission("FOO", "bar");

        // then
        assertThat(permission, equalTo(Optional.of(Permission.REPO_READ)));
    }

    @Test
    public void testGetCurrentUserRepositoryPermissionHasNoPermission() throws Exception {
        // given
        HttpExecutorMock.from(httpExecutor)
                .nextResponse(401, "{error : [{message:'No REPO_ADMIN permission'}]}")
                .nextResponse(401, "{error : [{message:'No REPO_READ permission'}]}");

        // when
        final Optional<Permission> permission = stashClient.getCurrentUserRepositoryPermission("FOO", "bar");

        // then
        assertThat(permission, equalTo(Optional.<Permission>empty()));
    }
}
