package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.core.entity.Link;
import com.google.common.base.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

class LinkParser implements Function<JsonElement, Link> {
    private final String hrefProperty;
    private final String nameProperty;

    public LinkParser(String hrefProperty, String nameProperty) {
        this.hrefProperty = hrefProperty;
        this.nameProperty = nameProperty;
    }

    @Override
    public Link apply(final JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        String href = jsonObject.has(hrefProperty) ? jsonObject.get(hrefProperty).getAsString() : "";
        String name = nameProperty != null && jsonObject.has(nameProperty) ? jsonObject.get(nameProperty).getAsString() : null;
        return new Link(href, name);
    }

}
